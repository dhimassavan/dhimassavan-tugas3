import {View, Text, ScrollView} from 'react-native';
import React from 'react';
import DtPicker from './screen/DtPicker';

import SliderCoba from './screen/SliderCoba';
import Geo from './screen/Geo';
import Progresview from './screen/Progresview';
import ProgresBar from './screen/ProgresBar';
import ClipBoard from './screen/ClipBoard';
import AysnStorage from './screen/AysnStorage';
import MyCarousel from './screen/MyCarousel';
import LinierGradient from './screen/LinierGradient';
import CobaShare from './screen/CobaShare';
import Skeleton from './screen/Skeleton';
import Botnav from './screen/Botnav';
import Rntools from './screen/Rntools';





const App = () => {
  return (
    <View style={{flex:1}}>
      <ScrollView>
      <DtPicker/>
      <SliderCoba/>
      <Geo/>
      <Progresview/>
      <ProgresBar/>
      <ClipBoard/>
      <AysnStorage/> 
      <MyCarousel/>
      <LinierGradient/>
      <CobaShare/>
      <Skeleton/>
      <Botnav/>
      <Rntools/>
      </ScrollView> 
    </View>
  );
};

export default App;
