import React, { useEffect, useState } from 'react';
import { View, Text } from 'react-native';
import Geolocation from '@react-native-community/geolocation';

const Geo = () => {
    const [latitude, setLatitude] = useState(null);
    const [longitude, setLongitude] = useState(null);
    const [error, setError] = useState(null);
  
    useEffect(() => {
      // Mendapatkan lokasi pengguna
      Geolocation.getCurrentPosition(
        (position) => {
          const { latitude, longitude } = position.coords;
          setLatitude(latitude);
          setLongitude(longitude);
        },
        (error) => {
          setError(error.message);
        },
        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
      );
    }, []);
  
    if (error) {
      return <Text>Error: {error}</Text>;
    }
  
    if (latitude && longitude) {
      return (
        <View>
          <Text>Lokasi Pengguna:</Text>
          <Text>Latitude: {latitude}</Text>
          <Text>Longitude: {longitude}</Text>
        </View>
      );
    }
  
    return <Text>Mendapatkan lokasi...</Text>;
  };

export default Geo