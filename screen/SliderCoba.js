import React, { Component } from 'react';
import Slider from '@react-native-community/slider'
import { View, Text, StyleSheet } from 'react-native';

class SliderCoba extends Component {
  //Buat State
       constructor(props) {
     super(props);
     this.state = {
       sliderValue: 15
     };
   }
   
    render() {
      return (
          //Code Return
        <View style={styles.container}>
          <Text style={{color: 'black'}}>Value of slider is :             {this.state.sliderValue}</Text>
          <Slider 
            maximumValue={100}
            minimumValue={0}
            minimumTrackTintColor="#307ecc"
            maximumTrackTintColor="#000000"
            step={1} 
            value={this.state.sliderValue}
            onValueChange={(sliderValue) => this.setState({ sliderValue })}
          />
        
        </View>
      );
    }
  }
  //Buat Style
  const styles = StyleSheet.create({
   container: {
     flex: 1,
     padding:20,
     justifyContent: 'center',
     backgroundColor: '#ecf0f1',
   }
  });
  export default SliderCoba