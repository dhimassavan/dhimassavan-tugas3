import { View, Text } from 'react-native'
import React from 'react'
import {ProgressView} from "@react-native-community/progress-view";

const Progresview = () => {
  return (
    <View>
      <ProgressView
          progressTintColor="orange"
          trackTintColor="blue"
          progress={0.7}
/>
    </View>
  )
}

export default Progresview