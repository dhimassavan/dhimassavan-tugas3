import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import Tooltip from 'rn-tooltip';

const Rntools = () =>{
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <TouchableOpacity>
          <Text>Show Tooltip</Text>
        </TouchableOpacity>
        <Tooltip
          popover={<Text>Tooltip Content</Text>}
          backgroundColor="gray"
        >
          <Text>Press Me</Text>
        </Tooltip>
      </View>
    );
  };
  

export default Rntools