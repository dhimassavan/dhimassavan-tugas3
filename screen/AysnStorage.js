import React, { useEffect, useState } from 'react';
import { Button, StyleSheet, Text, TextInput, View } from 'react-native';

const AysnStorage = () => {
    const [name, setName] = useState('');
    const [hobby, setHobby] = useState('');
    const [textName, setTextName] = useState('');
    const [textHobby, setTextHobby] = useState('');
  
    useEffect(() => {
      // Mengambil data dari AsyncStorage saat komponen pertama kali dirender
      fetchData();
    }, []);
  
    const fetchData = async () => {
      try {
        const storedName = await AsyncStorage.getItem('name');
        const storedHobby = await AsyncStorage.getItem('hobby');
        if (storedName !== null && storedHobby !== null) {
          setName(storedName);
          setHobby(storedHobby);
        }
      } catch (error) {
        console.log(error);
      }
    };
  
    const handleSave = async () => {
      try {
        // Menyimpan data ke AsyncStorage
        await AsyncStorage.setItem('name', textName);
        await AsyncStorage.setItem('hobby', textHobby);
        setName(textName);
        setHobby(textHobby);
      } catch (error) {
        console.log(error);
      }
    };
  
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Halo! Kenalan yuk!</Text>
        <Text style={styles.instructions}>
          Nama: {name}{'\n'}
          Hobi: {hobby}
        </Text>
        <TextInput
          style={styles.textInput}
          onChangeText={setTextName}
          placeholder="Nama"
        />
        <TextInput
          style={styles.textInput}
          onChangeText={setTextHobby}
          placeholder="Hobi"
        />
        <Button title="Simpan" onPress={handleSave} />
      </View>
    );
  };
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'flex-start',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
      padding: 16,
      paddingTop: 32,
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
    textInput: {
      height: 35,
      backgroundColor: 'white',
      marginTop: 8,
      marginBottom: 8,
      borderWidth: 1,
      borderColor: 'grey',
      padding: 8,
    },
  });

export default AysnStorage