import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import { Header, Colors } from 'react-native/Libraries/NewAppScreen';

import LinearGradient from 'react-native-linear-gradient';

const LinierGradient = () => {
    const [colorTop, setColorTop] = useState('#000000');
    const [colorBottom, setColorBottom] = useState('#cccccc');
  
    const incrementColor = (color, step) => {
      const intColor = parseInt(color.substr(1), 16);
      const newIntColor = (intColor + step).toString(16);
      return `#${'0'.repeat(6 - newIntColor.length)}${newIntColor}`;
    };
  
    useEffect(() => {
      const interval = setInterval(() => {
        setColorTop(prevColorTop => incrementColor(prevColorTop, 1));
        setColorBottom(prevColorBottom => incrementColor(prevColorBottom, -1));
      }, 20);
      return () => {
        clearInterval(interval);
      };
    }, []);
  
    return (
      <View  style={{position:'relative'}}>
        <LinearGradient
          colors={[colorTop, colorBottom]}
          style={styles.gradient}
        />
        <Text style={styles.colorText}>{colorTop}</Text>
        <Text style={styles.colorText}>{colorBottom}</Text>
      </View>
    );
  };
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    gradient: {
      width: 200,
      height: 200,
    },
    colorText: {
      marginTop: 10,
      fontSize: 18,
      fontWeight: 'bold',
    },
  });
  

export default LinierGradient