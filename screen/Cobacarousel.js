import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import Carousel from 'react-native-snap-carousel';

const DATA = [
  {
    title: 'Aenean leo',
    body: 'Ut tincidunt tincidunt erat. Sed cursus turpis vitae tortor. Quisque malesuada placerat nisl. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.',
    imgUrl: 'https://picsum.photos/id/11/200/300',
  },
  {
    title: 'In turpis',
    body: 'Aenean ut eros et nisl sagittis vestibulum. Donec posuere vulputate arcu. Proin faucibus arcu quis ante. Curabitur at lacus ac velit ornare lobortis. ',
    imgUrl: 'https://picsum.photos/id/10/200/300',
  },
  {
    title: 'Lorem Ipsum',
    body: 'Phasellus ullamcorper ipsum rutrum nunc. Nullam quis ante. Etiam ultricies nisi vel augue. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc.',
    imgUrl: 'https://picsum.photos/id/12/200/300',
  },
];

const Cobacarousel = () => {
    
    const renderItem = ({ item }) => (
      <View style={styles.container}>
        <Image source={{ uri: item.imgUrl }} style={styles.image} />
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.body}>{item.body}</Text>
      </View>
    );
  
    return (
      <View style={styles.carouselContainer}>
        <Carousel
          data={DATA}
          renderItem={renderItem}
          sliderWidth={300}
          itemWidth={200}
          layout="default"
        />
      </View>
    );
  };
  
  const styles = StyleSheet.create({
    carouselContainer: {
      marginTop: 50,
    },
    container: {
      backgroundColor: 'white',
      borderRadius: 5,
      padding: 20,
      alignItems: 'center',
    },
    image: {
      width: 200,
      height: 200,
      marginBottom: 10,
    },
    title: {
      fontSize: 18,
      fontWeight: 'bold',
      marginBottom: 5,
    },
    body: {
      fontSize: 14,
      textAlign: 'center',
    },
  });
  

export default Cobacarousel